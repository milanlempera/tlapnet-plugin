var ngTlapnetKonfigurator = angular.module('TlapnetKonfigurator', ['ui.bootstrap', 'ngRoute'], function($routeProvider, $locationProvider) {
  $routeProvider.when('/', {
    templateUrl: TLAPNET_KONFIGURATOR_PLUGIN_URL + '/templates/configurator.html',
    controller: TlapnetKonfiguratorCtrl
  });
  $routeProvider.when('/Objednavka', {
    templateUrl: TLAPNET_KONFIGURATOR_PLUGIN_URL + '/templates/customer.html',
    controller: TlapnetKonfiguratorCtrl
  });
});

ngTlapnetKonfigurator.directive('displayInvalid', function($parse, $filter) {
       
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elm, attrs, model) {
        var displayed = false;
        scope.$watch(attrs.ngModel, function(newValue, oldValue, scope) {
          // only set once... on initial load
          if(displayed == false && oldValue != undefined){
            displayed = true;
            elm.val(model.$modelValue);
          }
        });
      }
    }
  })
 
ngTlapnetKonfigurator.factory('theService', function($location) {
  var data = {services : [], loaded : false};
  
  initData = function (services) {
    angular.forEach(services, function(service) {
      service.selectedPackage = null;
      if ($location.search().service === service.Id) {
        service.open = true;
      }
      angular.forEach(service.Tariffs, function(tariff) {
        if ($location.search().tariff === tariff.Id) {
          tariff.visible = true;
        }
        tariff.parent = service;
        angular.forEach(tariff.Packages, function(package) {
          package.selectedChannels = {};
          if ($location.search().package === package.Id) {
            services.currentPackage = package;
          }
          package.parent = tariff;
          angular.forEach(package.Prices, function(price) {
            if (price.Type === 'BEZNA_CENA') {
              package.salePrice = price;
            }
            price.parent = package;
          });
          angular.forEach(package.Channels, function(channel) {
            channel.parent = package;
          });
        });
      });
    });
  };
  
  return {
    payments: {
      count: 0,
      monthlyPayments: 0,
      totalPayments: 0,
      subscription: false
    },
    getServices: function(callback) { 
      if (!data.loaded) {
        initData(globalServices);
        data.currentPackage = globalServices.currentPackage;
        globalServices.currentPackage = null;
        data.services = globalServices;
        data.loaded = true;
      }
      
      callback(data);
    }
  };
});
 

function TlapnetKonfiguratorCtrl($scope, $location, theService) {
  $scope.EMAIL_REGEXP = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
  $scope.PHONE_REGEXP = /^\+?([0-9]{3})? ?[0-9]{3} ?[0-9]{3} ?[0-9]{3}$/;

  $scope.customer = {
    name: '',
    email: '@',
    phone: '+420 ',
    city: '',
    street: '',
    houseNumber: ''
  };
  
  $scope.payments = theService.payments; 
  $scope.shoppingCart = [];
  
  $scope.validateAndSend = function() {
    if ($scope.validate()) {
      document.getElementById("tlapnetForm").submit();
    }
  };

  $scope.validate = function() {
    var messages = [];
    
    if (!$scope.customer.email.match($scope.EMAIL_REGEXP)) {
      messages.push("Email není validní.");
    }
    
    if (!$scope.customer.phone.match($scope.PHONE_REGEXP)) {
      messages.push("Telefon není validní.");
    }
    if ($scope.getSelectedPackages().length === 0) {
      messages.push("Vaše objednávka neobsahuje žádné služby. Vyberte nejprve alespoň jedenu službu.");
    }
    
    if (messages.length > 0) {
    jQuery.fancybox(
            '<div class="modal-validation"><div class="message">'+messages.join('</div><div class="message">')+'</div><button onclick="jQuery.fancybox.close();">OK</button></div>',
            {
              'autoDimensions': true,
              'width': 'auto',
              'height': 'auto',
              'transitionIn': 'none',
              'transitionOut': 'none'
            }
    );
      return false;
    }
    return true;
  };
   
  $scope.$on('$locationChangeSuccess', function(event) {
    $scope.isReview = $location.path() === '/Objednavka';
  });
  
  $scope.getSelectedPackages = function() {
    selectedServices = [];
    
    angular.forEach($scope.data.services, function(service) {
      if (service.selectedPackage !== null) {
        selectedServices.push(service.selectedPackage);
      }
    });
    
    return selectedServices;
    
  };
  
  $scope.toggleService = function(service) {
    service.visible = !service.visible;
  };
  
  $scope.selectTariff = function (tariff) {
    tariff.visible = true;
    
    angular.forEach(tariff.parent.Tariffs, function(item) {
      if (item !== tariff) {
        item.visible = !tariff.visible;
      }
    });
  };

  $scope.showNoPriceMessage = function(package) {
    jQuery.fancybox(
            '<div class="modal-required">'+package.NoPriceMessage+'<button onclick="jQuery.fancybox.close();">OK</button></div>',
            {
              'autoDimensions': true,
              'width': 'auto',
              'height': 'auto',
              'transitionIn': 'none',
              'transitionOut': 'none'
            }
    );
  };
  
  $scope.showDependencyModal = function(message) {
    jQuery.fancybox(
            '<div class="modal-required">'+message+'<button onclick="jQuery.fancybox.close();">OK</button></div>',
            {
              'autoDimensions': true,
              'width': 'auto',
              'height': 'auto',
              'transitionIn': 'none',
              'transitionOut': 'none'
            }
    );
  };
  
  $scope.checkDependency = function(requiredSettings) {
    var service = $scope.data.services.filter(function(service) {
      return service.Id === requiredSettings.service;
    })[0];

    if (!service.selectedPackage || service.selectedPackage.parent.Id !== requiredSettings.tariff) {
      $scope.showDependencyModal(requiredSettings.message);
      return false;
    }
    
    return true;    
  };
  
  $scope.togglePackage = function(package) {
    if (!package.hasOwnProperty('Prices')) {
      $scope.showNoPriceMessage(package);
      return;
    }
    
    var isAdd = (package.parent.parent.selectedPackage !== package);
    if (package.hasOwnProperty('Require') && isAdd) {
        if (!this.checkDependency(package.Require)) {
          return;
        }
      }

    var oldPackage = package.parent.parent.selectedPackage;
    if (!this.checkDependencyOnChange(oldPackage, package, isAdd)) {
      return;
    }

    if (package.parent.parent.selectedPackage !== null) {
      package.parent.parent.selectedPackage.selected = false;
    }
    
    package.selected = isAdd;
    if (package.selected) {
      package.parent.parent.selectedPackage = package;
    } else {
      angular.forEach(package.selectedChannels, function(channel) {
        $scope.toggleChannel(channel);
      });
      package.parent.parent.selectedPackage = null;
    }
            
   $scope.sumPayments();
  };

  $scope.getCurrentDependencies = function() {
    var dependencies = [];
    angular.forEach($scope.data.services, function(service) {
      if (service.selectedPackage && service.selectedPackage.Require) {
        var dependency = service.selectedPackage.Require;
        dependency.package = service.selectedPackage;
        dependencies.push(dependency);
      }
    });

    return dependencies;
  }

  $scope.checkDependencyOnChange = function(oldPackage, newPackage, isAdd) {
    var dependencies = $scope.getCurrentDependencies();
    for (index = 0; index < dependencies.length; ++index) {
      var dependency = dependencies[index];
      if (dependency.service === oldPackage.parent.parent.Id) {
        if (!isAdd || dependency.tariff !== newPackage.parent.Id) {
          $scope.showDependencyModal(dependency.message);
          return false;
        }
      }
    }

    return true;
  };
  
  
  $scope.toggleChannel = function (channel) {
    if (typeof channel.parent.selectedChannels[channel.Id] === "undefined") {
      channel.selected = true;
      channel.parent.selectedChannels[channel.Id] = channel;
    } else {
      channel.selected = false;
      delete channel.parent.selectedChannels[channel.Id];
    }
    channel.parent.channelSelected = (Object.keys(channel.parent.selectedChannels).length > 0);
    
    $scope.sumPayments();
  };
  
  $scope.removeChannelsOfPackage = function (package) {
    angular.forEach(package.selectedChannels, function(channel) {
      $scope.toggleChannel(channel);
    });
    $scope.sumPayments();
  };
  
  $scope.sumPayments = function () {
    $scope.setSalePrices();
    $scope.payments.monthlyPayments = 0;
    $scope.payments.count = 0;
    
    angular.forEach($scope.data.services, function(service) {
      if (service.selectedPackage !== null) {
        $scope.payments.count++;
        $scope.payments.monthlyPayments += parseInt(service.selectedPackage.salePrice.MonthlyPayment);
        angular.forEach(service.selectedPackage.selectedChannels, function(channel) {
          $scope.payments.monthlyPayments += parseInt(channel.Price);
        });
      }
    });
    $scope.payments.totalPayments = $scope.payments.monthlyPayments;
  };
  
  $scope.setSalePrices = function () {
    var selectedPackages = [];
    angular.forEach($scope.data.services, function(service) {
      if (service.selectedPackage !== null) {
        selectedPackages.push(service.selectedPackage);
      }
    });
    
    var priceType = 'BEZNA_CENA';
    if ($scope.payments.subscription) {
      if (selectedPackages.length > 1) {
        priceType = "PREDPLATNE_V_BALICKU";
      } else {
        priceType = "PREDPLATNE";
      }
    } else {
      if (selectedPackages.length > 1) {
        priceType = "BEZNA_CENA_V_BALICKU";
      } else {
        priceType = "BEZNA_CENA";
      }
    }
    
    angular.forEach(selectedPackages, function(package) {
      angular.forEach(package.Prices, function(price) {
        if (price.Type === priceType) {
          package.salePrice = price;
        }
        price.parent = package;
      });
    });
    
  };
  
  theService.getServices(function(data) {
    $scope.data = data;
    if (data.currentPackage) {
      $scope.togglePackage(data.currentPackage);
      data.currentPackage = null;
    }
    $scope.sumPayments();
  });
  
  
}
